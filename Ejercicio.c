#include<stdio.h>
#include<stdlib.h>

struct nodo{
int info;
struct nodo *sig;
};

struct nodo *raiz=NULL;

void insertar(int x){
	struct nodo *nuevo;
	nuevo=malloc(sizeof(struct nodo));
	nuevo->info=x;
	if(raiz==NULL){
		raiz=nuevo;
		nuevo->sig=NULL;
	}else{
		nuevo->sig=raiz;
		raiz=nuevo;
	}
}

void imprimir(){
	struct nodo *reco=raiz;
	printf("Lista completa. \n");
	while(reco!=NULL){
	printf("%i", reco->info);
	reco=reco->sig;
	}
	printf("\n");
	
}

int extraer(){
	if(raiz!=NULL){
	int informacion=raiz->info;
	struct nodo *bor=raiz;
	raiz=raiz->sig;
	free(bor);
	return informacion;
	}else{
	return -1;
	}
}

void liberar(){
	struct nodo *reco=raiz;
	struct nodo *bor;
	while(reco!=NULL){
	bor=reco;
	reco=reco->sig;
	free(bor);
	}
}

//Retorna la cantidad de nodos de la pila
int cantidadNodos()
{
    struct nodo *reco = raiz;
    int cant = 0;
   //Mientras la pila no este vacia mostrara el total de nodos registrados
    while (reco != NULL)
    {
        cant++;
        reco = reco->sig;
    }
    return cant;
}

//Metodo para indicar si la pila esta vacia
int pilaVacia()
{
	//Si esta vacia retornara 1 y sino retornara 0
    if (raiz == NULL)
        return 1;
    else
        return 0;
}



int main(){
insertar(10);
insertar(40);
insertar(3);
imprimir();
printf("Extraemos de la pila: %i\n", extraer());
imprimir();
liberar();
printf("La cantidad total de nodos de la pila es:%i\n",cantidadNodos());
    while (pilaVacia() == 0)
    {
        printf("Extraemos de la pila:%i\n",extraer());
    }
return 0;
}



